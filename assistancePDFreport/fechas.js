/*Javascript las fechas son calculadas en milisegundos desde el
el 01 de enero de 1970 */
var minutes = 1000 * 60;
var hours = minutes * 60;
var days = hours * 24;
var years = days * 365;
var d = new Date();
var t = d.getTime();//fecha en milisegundos dese 1970
/*Manejo de fechas mes/dia/año*/
/*horas en utc Universal time*/
var hoy = new Date("03/12/2017 03:21:01");
var ayer = new Date("03/12/2017 23:21:00");

var h = new Date("13/02/2017 08:59:00");
console.log(`--------------------------> ${h.getTime()}`);

console.log(Math.round(hoy.getTime()/days));
var dias = Math.round(hoy.getTime()/days) - Math.round(ayer.getTime()/days);
console.log("Diferencia de dias es "+dias);

if(hoy.getTime() > ayer.getTime()){
	console.log("hoy es mayor");
}
else{
	console.log("ayer es mayor");
}
console.log("Dia de la semana: "+hoy,getDay());
console.log("Dia: "+hoy.getDate());
console.log("Mes: "+hoy.getMonth());
console.log("Año: "+hoy.getFullYear());
console.log("Horas: "+hoy.getHours());
console.log("Minutos: "+hoy.getMinutes());
console.log("Segundos: "+hoy.getSeconds());

/*Números infinitos*/

var bigNumber = Number.POSITIVE_INFINITY;
var x = 99999999999999999999999999999999999999999999999999999999999;

var smallNumber = Number.NEGATIVE_INFINITY;
var y = 1;

if(bigNumber > x){
	console.log("BigNumber es mayor");
}
else{
	console.log("X es mayor");
}

if(smallNumber < y){
	console.log("Y es mayor");
}
else{
	console.log("SmallNumber es menor");
}